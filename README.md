---
license: creativeml-openrail-m
tags:
- text generation
- conversational
- gptq
- 4bit
inference: false
language:
- en
pipeline_tag: text-generation
---

GPTQ quantization of https://huggingface.co/PygmalionAI/pygmalion-6b/commit/b8344bb4eb76a437797ad3b19420a13922aaabe1

Using this repository: https://github.com/mayaeary/GPTQ-for-LLaMa/tree/gptj-v2

Command: 
```
python3 gptj.py models/pygmalion-6b_b8344bb4eb76a437797ad3b19420a13922aaabe1 c4 --wbits 4 --groupsize 128 --save_safetensors models/pygmalion-6b-4bit-128g.safetensors
```